package edu.ntnu.idatt2001.util;

import edu.ntnu.idatt2001.data.action.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ActionFactoryTest {
    @Test
    void healthStringShouldReturnHealthAction() {
        String actionString = "Health:100";

        Action testAction = new HealthAction(100);
        Action factoryAction = ActionFactory.createActionFromString(actionString);
        Action negativeTestAction = new GoldAction(100);

        assertEquals(testAction.getClass(), factoryAction.getClass());
        assertNotEquals(negativeTestAction.getClass(), factoryAction.getClass());
    }

    @Test
    void goldStringShouldReturnGoldAction() {
        String actionString = "Gold:100";

        Action testAction = new GoldAction(100);
        Action factoryAction = ActionFactory.createActionFromString(actionString);
        Action negativeTestAction = new HealthAction(100);

        assertEquals(testAction.getClass(), factoryAction.getClass());
        assertNotEquals(negativeTestAction.getClass(), factoryAction.getClass());
    }

    @Test
    void scoreStringShouldReturnScoreAction() {
        String actionString = "Score:100";

        Action testAction = new ScoreAction(100);
        Action factoryAction = ActionFactory.createActionFromString(actionString);
        Action negativeTestAction = new HealthAction(100);

        assertEquals(testAction.getClass(), factoryAction.getClass());
        assertNotEquals(negativeTestAction.getClass(), factoryAction.getClass());
    }

    @Test
    void itemStringShouldReturnItemAction() {
        String actionString = "Item:Sword";

        Action testAction = new ItemAction("Sword");
        Action factoryAction = ActionFactory.createActionFromString(actionString);
        Action negativeTestAction = new HealthAction(100);

        assertEquals(testAction.getClass(), factoryAction.getClass());
        assertNotEquals(negativeTestAction.getClass(), factoryAction.getClass());
    }

    @Test
    void emptyInputShouldReturnNull() {
        String actionString = "";
        Action factoryAction = ActionFactory.createActionFromString(actionString);

        assertNull(factoryAction);
    }

    @Test
    void invalidFormatShouldReturnNull() {
        String actionString = "Item::Sword";
        Action factoryAction = ActionFactory.createActionFromString(actionString);

        assertNull(factoryAction);
    }

    @Test
    void unsupportedTypeShouldReturnNull() {
        String actionString = "Attack:100";
        Action factoryAction = ActionFactory.createActionFromString(actionString);

        assertNull(factoryAction);
    }

    @Test
    void nonIntInputForIntGoalsShouldReturnNull() {
        String actionString = "Gold:test";
        Action factoryAction = ActionFactory.createActionFromString(actionString);

        assertNull(factoryAction);
    }
}
