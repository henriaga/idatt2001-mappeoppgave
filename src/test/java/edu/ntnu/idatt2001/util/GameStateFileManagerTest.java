package edu.ntnu.idatt2001.util;

import edu.ntnu.idatt2001.data.*;
import edu.ntnu.idatt2001.data.goal.Goal;
import edu.ntnu.idatt2001.data.goal.GoldGoal;
import edu.ntnu.idatt2001.data.goal.HealthGoal;
import edu.ntnu.idatt2001.data.goal.ScoreGoal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GameStateFileManagerTest {

    private Game game;
    private Story story;
    private Player player;
    private List<Goal> goals;

    @BeforeEach
    public void setUp() {
        player = new Player.Builder("Player1")
                .health(200)
                .score(100)
                .gold(30)
                .build();

        story = new Story("Test Story");

        Passage openingPassage = new Passage("Opening", "This is the content of the opening passage.");
        Passage passage1 = new Passage("Passage 1", "This is the content of passage 1.");
        Passage passage2 = new Passage("Passage 2", "This is the content of passage 2.");
        story.setOpeningPassage(openingPassage);
        story.addPassage(passage1);

        Link link = new Link("Link to passage 1", "Opening");
        Link link1 = new Link("Link to passage 2", "Passage 1");
        openingPassage.addLink(link);
        passage1.addLink(link1);

        goals = new ArrayList<>();
        goals.add(new GoldGoal(300));
        goals.add(new ScoreGoal(300));
        goals.add(new HealthGoal(300));

        game = new Game(player, story, goals);
    }

    @Test
    public void testSaveAndLoadGameState() throws IOException, ClassNotFoundException {
        String path = "src/test/resources/saves/test_gameState.txt";
        GameStateFileManager.saveGameState(game, path);
        Game loadedGameState = GameStateFileManager.loadGameState(path);

        assertEquals(game.getPlayer().getName(), loadedGameState.getPlayer().getName());
        assertEquals(game.getStory().getTitle(), loadedGameState.getStory().getTitle());
        assertEquals(game.getStory().getPassages().size(), loadedGameState.getStory().getPassages().size());
        assertEquals(game.getStory().getOpeningPassage().getTitle(), loadedGameState.getStory().getOpeningPassage().getTitle());
        assertEquals(game.getStory().getOpeningPassage().getContent(), loadedGameState.getStory().getOpeningPassage().getContent());
        assertEquals(game.getGoals().size(), loadedGameState.getGoals().size());
    }
}
