package edu.ntnu.idatt2001.util;

import edu.ntnu.idatt2001.data.Link;
import edu.ntnu.idatt2001.data.Passage;
import edu.ntnu.idatt2001.data.Story;
import edu.ntnu.idatt2001.data.action.ScoreAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class StoryFileManagerTest {

    private Story story;

    @BeforeEach
    public void setUp() {
        story = new Story("Test Story");

        Passage passage1 = new Passage("Passage 1", "This is the content of passage 1.");
        Passage passage2 = new Passage("Passage 2", "This is the content of passage 2.");
        Passage passage3 = new Passage("Passage 3", "This is the content of passage 3.");
        Passage passage4 = new Passage("Passage 4", "This is the content of passage 4.");
        story.addPassage(passage1);
        story.addPassage(passage2);
        story.addPassage(passage3);
        story.addPassage(passage4);
        story.setOpeningPassage(passage1);

        Link link1 = new Link("Link to passage 2", "Passage 1");
        Link link2 = new Link("Link to passage 3", "Passage 2");
        Link actionLink = new Link("Link to passage 4", "Passage 3");

        actionLink.addAction(new ScoreAction(100));

        passage1.addLink(link1);
        passage2.addLink(link2);
        passage3.addLink(actionLink);
    }

    @Test
    public void testSaveAndLoadStory(@TempDir Path tempDir) throws IOException {
        Path storyFile = tempDir.resolve("test_story");

        // Save the story
        StoryFileManager.saveStory(story, storyFile);

        // Load the story
        Story loadedStory = StoryFileManager.loadStory(storyFile);

        // Assert that the loaded story is not null and has the same title as the original story
        assertNotNull(loadedStory);
        assertEquals(story.getTitle(), loadedStory.getTitle());

        // Compare passages and links
        assertEquals(story.getPassages().size(), loadedStory.getPassages().size());
        assertEquals(story.getOpeningPassage().getLinks().size(), loadedStory.getOpeningPassage().getLinks().size());
        assertEquals(story.getOpeningPassage().getTitle(), loadedStory.getOpeningPassage().getTitle());
        assertEquals(story.getOpeningPassage().getContent(), loadedStory.getOpeningPassage().getContent());
        assertEquals(story.getOpeningPassage().getLinks().get(0).getText(), loadedStory.getOpeningPassage().getLinks().get(0).getText());
        assertEquals(story.getOpeningPassage().getLinks().get(0).getReference(), loadedStory.getOpeningPassage().getLinks().get(0).getReference());


    }
}
