package edu.ntnu.idatt2001.data;

import edu.ntnu.idatt2001.data.goal.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GameTest {
    private final Player testPlayer = new Player.Builder("Player1")
            .health(200)
            .score(100)
            .gold(30)
            .build();

    private final Passage testPassage = new Passage("Forest", "You have reached a forest.");
    private final Story testStory = new Story("Story1", testPassage);
    private final Goal testGoldGoal = new GoldGoal(200);
    private final Goal testHealthGoal = new HealthGoal(300);
    private final Goal testInventoryGoal = new InventoryGoal(List.of("Sword", "Steak", "Orb"));
    private final Goal testScoreGoal = new ScoreGoal(350);
    private final List<Goal> gameGoals = new ArrayList<>(List.of(testGoldGoal, testHealthGoal, testInventoryGoal, testScoreGoal));

    private final Game testGame = new Game(testPlayer, testStory, gameGoals);

    @BeforeEach
    void setUp() {
        testGame.setCurrentPassage(testPassage);
    }


    @Nested
    class Getters {
        @Test
        void getPlayerShouldReturnPlayer() {
            assertEquals(testPlayer, testGame.getPlayer());
        }

        @Test
        void getStoryShouldReturnStory() {
            assertEquals(testStory, testGame.getStory());
        }

        @Test
        void getGoalsShouldReturnListOfGoals() {
            assertEquals(gameGoals, testGame.getGoals());
        }

        @Test
        void getCurrentPassageShouldReturnCurrentPassage() {
            assertEquals(testPassage, testGame.getCurrentPassage());
        }
    }

    @Test
    void beginShouldReturnOpeningPassage() {
        assertEquals(testPassage, testGame.begin());
    }

    @Test
    void goShouldGoToPassageWithLink() {
        Passage passage = new Passage("Field", "Content");
        testStory.addPassage(passage);

        assertEquals(passage, testGame.go(new Link(passage.getTitle(), passage.getTitle())));
    }

    @Test
    void setCurrentPassageShouldUpdateCurrentPassage() {
        Passage currentPassage = new Passage("Current", "Current");
        testGame.setCurrentPassage(currentPassage);

        assertEquals(currentPassage, testGame.getCurrentPassage());
    }
}