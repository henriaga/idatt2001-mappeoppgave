package edu.ntnu.idatt2001.data;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class PassageTest {
    private Passage testPassage;
    private Link testLink1;
    private Link testLink2;

    @BeforeEach //Gjøre det i field i stedet?
    void setUp() {
        testPassage = new Passage("Forest", "You have reached a forest.");
        testLink1 = new Link("Run", "Field");
        testLink2 = new Link("Hide", "Cave");
    }

    @Nested
    class Getters {
        @Test
        void getTitleShouldReturnTitle() {
            assertEquals("Forest", testPassage.getTitle());
        }

        @Test
        void getContentShouldReturnContent() {
            assertEquals("You have reached a forest.", testPassage.getContent());
        }

        @Test
        void getLinksShouldReturnListOfLinks() {
            List<Link> linkList = new ArrayList<>();
            linkList.add(testLink1);
            linkList.add(testLink2);

            testPassage.addLink(testLink1);
            testPassage.addLink(testLink2);

            assertEquals(linkList, testPassage.getLinks());
        }
    }

    @Test
    void addLinkShouldAddNewLink() {
        testPassage.addLink(testLink1);
        assertTrue(testPassage.getLinks().contains(testLink1));
    }

    @Nested
    class HasLinks {
        @Test
        void hasLinksShouldReturnTrueIfHasLinks() {
            testPassage.addLink(testLink1);
            assertTrue(testPassage.hasLinks());
        }

        @Test
        void hasLinkShouldReturnFalseIfNoLinks() {
            assertFalse(testPassage.hasLinks());
        }
    }

    @Test
    void toStringShouldOutputCorrectInfo() {
        assertEquals("Passage{" +
                "title='" + "Forest" + '\'' +
                ", content='" + "You have reached a forest." + '\'' +
                ", links=" + "[]}", testPassage.toString());
    }

    @Test
    void hashCodeTest() {
        int hashCode = Objects.hash(testPassage.getTitle(), testPassage.getContent(), testPassage.getLinks());

        assertEquals(hashCode, testPassage.hashCode());
    }
}