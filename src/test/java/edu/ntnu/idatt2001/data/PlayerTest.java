package edu.ntnu.idatt2001.data;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PlayerTest {
    private final Player testPlayer = new Player.Builder("Player1")
            .health(200)
            .score(100)
            .gold(30)
            .build();

    @Nested
    class Constructor {
        @Test
        void playerNegativeHealthShouldThrowException() {
            Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
                new Player.Builder("Player1")
                        .health(-1)
                        .score(100)
                        .gold(30)
                        .build();
            });

            assertEquals("Health cannot be less than or equal to 0.", exception.getMessage());
        }

        @Test
        void playerNegativeScoreShouldThrowException() {
            Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
                new Player.Builder("Player1")
                        .health(200)
                        .score(-1)
                        .gold(30)
                        .build();
            });

            assertEquals("Score cannot be less than 0.", exception.getMessage());
        }

        @Test
        void playerNegativeGoldShouldThrowException() {
            Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
                new Player.Builder("Player1")
                        .health(200)
                        .score(100)
                        .gold(-1)
                        .build();
            });

            assertEquals("Gold cannot be less than 0.", exception.getMessage());
        }
    }

    @Nested
    class Getters {
        @Test
        void getNameShouldReturnName() {
            assertEquals("Player1", testPlayer.getName());
        }

        @Test
        void getHealthShouldReturnHealth() {
            assertEquals(200, testPlayer.getHealth());
        }

        @Test
        void getScoreShouldReturnScore() {
            assertEquals(100, testPlayer.getScore());
        }

        @Test
        void getGoldShouldReturnGold() {
            assertEquals(30, testPlayer.getGold());
        }

        @Test
        void getInventoryShouldReturnListOfItems() {
            String item1 = "Sword";
            String item2 = "Wand";
            String item3 = "Orb";
            String item4 = "Steak";

            List<String> itemList = new ArrayList<>(Arrays.asList(item1, item2, item3, item4));
            testPlayer.addToInventory(item1);
            testPlayer.addToInventory(item2);
            testPlayer.addToInventory(item3);
            testPlayer.addToInventory(item4);

            assertEquals(itemList, testPlayer.getInventory());
        }
    }

    @Nested
    class Adders {
        @Nested
        class AddHealth {
            @Test
            void addHealthPositiveValueShouldIncreaseHealth() {
                testPlayer.addHealth(20);
                assertEquals(220, testPlayer.getHealth());
            }

            @Test
            void addHealthNegativeValueShouldDecreaseHealth() {
                testPlayer.addHealth(-20);
                assertEquals(180, testPlayer.getHealth());
            }

            @Test
            void addHealthRemoveMoreThanCurrentShouldSetToZero() {
                testPlayer.addHealth(-2000);
                assertEquals(0, testPlayer.getHealth());
            }
        }

        @Nested
        class AddScore {
            @Test
            void addScorePositiveValueShouldIncreaseScore() {
                testPlayer.addScore(20);
                assertEquals(120, testPlayer.getScore());
            }

            @Test
            void addScoreNegativeValueShouldDecreaseScore() {
                testPlayer.addScore(-20);
                assertEquals(80, testPlayer.getScore());
            }

            @Test
            void addScoreRemoveMoreThanCurrentShouldSetToZero() {
                testPlayer.addScore(-1000);
                assertEquals(0, testPlayer.getScore());
            }
        }

        @Nested
        class AddGold {
            @Test
            void addScorePositiveShouldIncreaseGold() {
                testPlayer.addGold(20);
                assertEquals(50, testPlayer.getGold());
            }

            @Test
            void addGoldNegativeValueShouldDecreaseGold() {
                testPlayer.addGold(-20);
                assertEquals(10, testPlayer.getGold());
            }

            @Test
            void addGoldRemoveMoreThanCurrentShouldSetToZero() {
                testPlayer.addGold(-100);
                assertEquals(0, testPlayer.getGold());
            }
        }

        @Test
        void addToInventoryShouldAddItem() {
            testPlayer.addToInventory("Sword");
            assertTrue(testPlayer.getInventory().contains("Sword"));
        }
    }

    @Nested
    class isAliveTests {
        @Test
        void isAliveHealthIsZeroShouldReturnFalse() {
            testPlayer.addHealth(-1000);
            assertFalse(testPlayer.isAlive());
        }

        @Test
        void isAliveHealthAboveZeroShouldReturnTrue() {
            assertTrue(testPlayer.isAlive());
        }
    }
}
