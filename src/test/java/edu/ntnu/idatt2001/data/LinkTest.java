package edu.ntnu.idatt2001.data;

import edu.ntnu.idatt2001.data.action.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LinkTest {
    private final Link testLink = new Link("Run", "Field");
    private final Action testGoldAction = new GoldAction(50);
    private final Action testScoreAction = new ScoreAction(150);
    private final Action testHealthAction = new HealthAction(100);
    private final Action testItemAction = new ItemAction("Sword");

    @Nested
    class Getters {
        @Test
        void getTextShouldReturnText() {
            assertEquals("Run", testLink.getText());
        }

        @Test
        void getReferenceShouldReturnReference() {
            assertEquals("Field", testLink.getReference());
        }

        @Test
        void getActionsShouldReturnListOfActions() {
            List<Action> actionList = new ArrayList<>();
            actionList.add(testGoldAction);
            actionList.add(testHealthAction);
            actionList.add(testScoreAction);
            actionList.add(testItemAction);

            testLink.addAction(testGoldAction);
            testLink.addAction(testHealthAction);
            testLink.addAction(testScoreAction);
            testLink.addAction(testItemAction);

            assertEquals(actionList, testLink.getActions());
        }
    }

    @Test
    void addActionShouldAddAction() {
        testLink.addAction(testGoldAction);
        assertTrue(testLink.getActions().contains(testGoldAction));
    }

    @Test
    void toStringShouldOutputCorrectInfo() {
        assertEquals("Link toString: Run, Field", testLink.toString());
    }
}
