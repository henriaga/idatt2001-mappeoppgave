package edu.ntnu.idatt2001.data.goal;

import edu.ntnu.idatt2001.data.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class HealthGoalTest {
    private final Goal healthGoal = new HealthGoal(100);
    private final Player testPlayerAboveGoal = new Player.Builder("Player1")
            .health(200)
            .score(100)
            .gold(220)
            .build();

    private final Player testPlayerBellowGoal = new Player.Builder("Player1")
            .health(50)
            .score(100)
            .gold(30)
            .build();

    @Test
    void ifFulfilledConditionMet() {
        assertTrue(healthGoal.isFulfilled(testPlayerAboveGoal));
    }

    @Test
    void isFulfilledConditionNotMet() {
        assertFalse(healthGoal.isFulfilled(testPlayerBellowGoal));
    }

    @Test
    void healthAmountLessThanZeroShouldThrowException() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
            new HealthGoal(-1);
        });

        assertEquals("Goal amount cannot be less than 0.", exception.getMessage());
    }
}
