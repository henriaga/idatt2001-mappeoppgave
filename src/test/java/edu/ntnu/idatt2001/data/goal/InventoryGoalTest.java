package edu.ntnu.idatt2001.data.goal;


import edu.ntnu.idatt2001.data.Player;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class InventoryGoalTest {
    List<String> mandatoryItems = List.of("item1", "item2", "item3");
    @Test
    public void testIsFulfilled() {

        Player player = new Player.Builder("Player1")
                .health(10)
                .score(10)
                .gold(10)
                .build();
        player.addToInventory("item1");
        player.addToInventory("item2");
        player.addToInventory("item3");
        InventoryGoal inventoryGoal = new InventoryGoal(mandatoryItems);
        assertTrue(inventoryGoal.isFulfilled(player));
    }

    @Test
    void goalNotFulfilledShouldReturnFalse() {
        Player player = new Player.Builder("Player1")
                .health(10)
                .score(10)
                .gold(10)
                .build();
        InventoryGoal inventoryGoal = new InventoryGoal(mandatoryItems);
        assertFalse(inventoryGoal.isFulfilled(player));
    }

    @Test
    void emptyGoalShouldThrowException() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
            new InventoryGoal(List.of());
        });

        assertEquals("MandatoryItems cannot be empty.", exception.getMessage());
    }

}
