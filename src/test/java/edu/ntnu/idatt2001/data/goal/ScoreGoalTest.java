package edu.ntnu.idatt2001.data.goal;

import edu.ntnu.idatt2001.data.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ScoreGoalTest {
    private final Goal scoreGoal = new ScoreGoal(150);
    private final Player testPlayerAboveGoal = new Player.Builder("Player1")
            .health(200)
            .score(200)
            .gold(220)
            .build();

    private final Player testPlayerBellowGoal = new Player.Builder("Player1")
            .health(200)
            .score(100)
            .gold(30)
            .build();

    @Test
    void ifFulfilledConditionMet() {
        assertTrue(scoreGoal.isFulfilled(testPlayerAboveGoal));
    }

    @Test
    void isFulfilledConditionNotMet() {
        assertFalse(scoreGoal.isFulfilled(testPlayerBellowGoal));
    }

    @Test
    void scoreAmountLessThanZeroShouldThrowException() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
            new ScoreGoal(-1);
        });

        assertEquals("Goal amount cannot be less than 0.", exception.getMessage());
    }
}
