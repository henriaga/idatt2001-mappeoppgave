package edu.ntnu.idatt2001.data.goal;

import edu.ntnu.idatt2001.data.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GoldGoalTest {
    private final Goal goldGoal = new GoldGoal(200);
    private final Player testPlayerAboveGoal = new Player.Builder("Player1")
            .health(200)
            .score(100)
            .gold(220)
            .build();

    private final Player testPlayerBellowGoal = new Player.Builder("Player1")
            .health(200)
            .score(100)
            .gold(30)
            .build();

    @Test
    void ifFulfilledConditionMet() {
        assertTrue(goldGoal.isFulfilled(testPlayerAboveGoal));
    }

    @Test
    void isFulfilledConditionNotMet() {
        assertFalse(goldGoal.isFulfilled(testPlayerBellowGoal));
    }

    @Test
    void goalAmountLessThanZeroShouldThrowException() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
            new GoldGoal(-1);
        });

        assertEquals("Goal amount cannot be less than 0.", exception.getMessage());
    }
}