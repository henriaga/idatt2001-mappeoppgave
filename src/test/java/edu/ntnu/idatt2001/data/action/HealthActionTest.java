package edu.ntnu.idatt2001.data.action;

import edu.ntnu.idatt2001.data.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HealthActionTest {
    private final Action testHealthAction = new HealthAction(100);
    private final Player testPlayer = new Player.Builder("Player1")
            .health(200)
            .score(100)
            .gold(30)
            .build();

    @Test
    void executeShouldAddToPlayersHealth() {
        testHealthAction.execute(testPlayer);
        assertEquals(300, testPlayer.getHealth());
    }
}