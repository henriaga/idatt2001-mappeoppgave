package edu.ntnu.idatt2001.data;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class StoryTest {

    private String title;
    private Map<Link, Passage> passages;
    private Passage openingPassage;

    @BeforeEach
    public void setUp() {
        title = "Test";
        passages = new HashMap<>();
        openingPassage = new Passage("Test", "Test");
    }

    @Nested
    public class Getters {

        @Test
        public void testGetTitle() {
            Story story = new Story(title, openingPassage);
            assert(story.getTitle().equals(title));
        }

        @Test
        public void testGetOpeningPassage() {
            Story story = new Story(title, openingPassage);
            assert(story.getOpeningPassage().equals(openingPassage));
        }

        @Test
        public void testGetPassage() {
            Story story = new Story(title, openingPassage);
            Link link = new Link("Test", "Test");
            story.addPassage(openingPassage);
            assertTrue(story.getPassage(link).equals(openingPassage));
        }

        @Test
        public void testGetPassages() {
            Story story = new Story(title, openingPassage);
            story.addPassage(openingPassage);
            assertTrue(story.getPassages().contains(new Passage("Test", "Test")));
        }
    }

    @Nested
    public class Adders {
        @Test
        public void testAddPassage() {
            Story story = new Story(title, openingPassage);
            story.addPassage(openingPassage);
            assertTrue(story.getPassages().contains(new Passage("Test", "Test")));
        }
    }

    @Test
    void brokenLinksTest() {
        Story story = new Story(title, openingPassage);
        Passage passage1 = new Passage("Test1", "Test1");
        Passage passage2 = new Passage("Test2", "Test2");

        Link broken1 = new Link("broken1", "Test3");
        Link broken2 = new Link("broken2", "test");
        Link working = new Link("working", "Test2");

        passage1.addLink(working);
        passage1.addLink(broken1);
        passage2.addLink(broken2);

        story.addPassage(passage1);
        story.addPassage(passage2);

        assertTrue(story.getBrokenLinks().contains(broken1) && story.getBrokenLinks().contains(broken2));
        assertFalse(story.getBrokenLinks().contains(working));
    }

    @Test
    void isPassageReferencedTest() {
        Story story = new Story(title, openingPassage);
        Passage passage1 = new Passage("Test1", "Test1");
        Passage passage2 = new Passage("Test2", "Test2");

        Link link = new Link("working", "Test2");

        passage1.addLink(link);

        story.addPassage(passage1);
        story.addPassage(passage2);

        assertTrue(story.isPassageReferenced(passage2));
        assertFalse(story.isPassageReferenced(passage1));
    }

    @Test
    void removePassageTest() {
        Story story = new Story(title, openingPassage);
        Passage passage1 = new Passage("Test1", "Test1");
        Passage passage2 = new Passage("Test2", "Test2");

        Link link = new Link("working", "Test2");

        passage1.addLink(link);

        story.addPassage(passage1);
        story.addPassage(passage2);

        assertTrue(story.removePassage(new Link("Test1", "Test1")));
        assertFalse(story.removePassage(new Link("Test2", "Test2")));
    }
}
