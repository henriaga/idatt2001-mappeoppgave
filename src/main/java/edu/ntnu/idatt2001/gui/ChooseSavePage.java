package edu.ntnu.idatt2001.gui;

import edu.ntnu.idatt2001.data.Game;
import edu.ntnu.idatt2001.util.CommonStyles;
import edu.ntnu.idatt2001.util.GameStateFileManager;
import edu.ntnu.idatt2001.util.StoryFileManager;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class for choosing a save file.
 * The page contains a list of all the save files in the save folder.
 * The player can select a save file and load it.
 */
public class ChooseSavePage extends Application {
    private Stage stage;

    private final ListView<String> savedGamesListView = new ListView<>();

    /**
     * Displays the save selection page.
     *
     * @param stage The stage the page is displayed on.
     */
    @Override
    public void start(Stage stage) {
        this.stage = stage;
        VBox rootNode = new VBox(15);

        fillSavedGamesList();

        Text pageTitle = new Text("Choose save");
        Text debuggingInfo = new Text("Note: If your saves aren't showing check if they are correctly saved. "
                + "They should be under: src/main/resources/saves");

        rootNode.setAlignment(Pos.TOP_CENTER);
        pageTitle.setStyle("-fx-font: 24 arial");
        debuggingInfo.setStyle("-fx-font: 16 arial");

        Button selectButton = new Button("Select");
        selectButton.setOnAction(e -> {
            String selectedSave = savedGamesListView.getSelectionModel().getSelectedItem();
            if (selectedSave != null) {
                loadGame(selectedSave);
                stage.close();
            }
        });

        Button backButton = new Button("Back");
        backButton.setOnAction(e -> {
            StartPage startPage = new StartPage();
            startPage.start(new Stage());
            stage.close();
        });

        CommonStyles.setButtonStyle(14, selectButton, backButton);
        List<Button> buttons = List.of(selectButton, backButton);
        buttons.forEach(button -> {
            button.prefWidthProperty().bind(Bindings.divide(stage.widthProperty(), 14));
            button.prefHeightProperty().bind(Bindings.divide(stage.heightProperty(), 20));
        });

        rootNode.getChildren().addAll(pageTitle, savedGamesListView, selectButton, backButton, debuggingInfo);

        Scene scene = new Scene(rootNode);
        stage.setMaximized(true);
        stage.setTitle("Saved games");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Fills in the list with the saved games.
     */
    public void fillSavedGamesList() {
        try {
            Path saveDir = Paths.get("src/main/resources/saves");
            savedGamesListView.getItems().setAll(Files.list(saveDir)
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .collect(Collectors.toList()

            ));
        } catch (IOException e) {
            StartPage startPage = new StartPage();
            startPage.start(new Stage());
            stage.close();

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Page Loading Error");
            alert.setHeaderText("Error loading page.");
            alert.setContentText("The game might be incorrectly installed.");
            alert.showAndWait();
        }

    }

    /**
     * Loads a game save.
     *
     * @param saveFileName The name of the savefile.
     */
    private void loadGame(String saveFileName) {
        try {
            Game game = GameStateFileManager.loadGameState("src/main/resources/saves/" + saveFileName);
            GamePage gamePage = new GamePage(game);
            gamePage.start(new Stage());
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Loading Error");
            alert.setHeaderText("Error loading game.");
            alert.setContentText("The saves might be save incorrectly. Check if the save is in the saves folder.");
            alert.showAndWait();
        }
    }
}


