package edu.ntnu.idatt2001.gui;

import edu.ntnu.idatt2001.util.CommonStyles;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.List;

/**
 * The start page for the game.
 * The page contains buttons for starting a new game, continuing a game and exiting the game.
 * The page is the first page the player sees when starting the game.
 */
public class StartPage extends Application {
    /**
     * Displays the startpage.
     *
     * @param stage The stage the page is displayed on.
     */
    @Override
    public void start(Stage stage) {
        VBox rootNode = new VBox(15);

        Text gameTitle = new Text("Paths");
        Button continueButton = new Button("Continue");
        Button newGameButton = new Button("New Game");
        Button exitButton = new Button("Exit");

        //Switches scene to the main page.
        continueButton.setOnAction(event -> {
            System.out.println("Continues from StartPage to ChooseSavePage");
            ChooseSavePage chooseSavePage = new ChooseSavePage();
            chooseSavePage.start(new Stage());
            stage.close();
        });

        //Switches scene to the create goals page.
        newGameButton.setOnAction(event -> {
            SelectStoryPage selectStoryPage = new SelectStoryPage();
            selectStoryPage.start(new Stage());
            stage.close();
        });

        exitButton.setOnAction(event -> Platform.exit());


        gameTitle.setStyle("-fx-font: 50 arial; -fx-font-weight: bold");

        //Sets the style of the buttons
        List<Button> buttons = List.of(continueButton, newGameButton, exitButton);
        buttons.forEach(button -> {
            button.prefHeightProperty().bind(Bindings.divide(stage.heightProperty(), 16));
            button.prefWidthProperty().bind(Bindings.divide(stage.widthProperty(), 4));
        });
        CommonStyles.setButtonStyle(24, continueButton, newGameButton, exitButton);

        rootNode.getChildren().addAll(gameTitle, continueButton, newGameButton, exitButton);
        rootNode.setAlignment(Pos.CENTER);

        Scene scene = new Scene(rootNode);
        stage.setMaximized(true);
        stage.setTitle("Game");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args The commandline arguments.
     */
    public static void main(String[] args) {
        launch(args);
    }
}
