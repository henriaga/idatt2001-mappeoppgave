package edu.ntnu.idatt2001.gui;

import edu.ntnu.idatt2001.data.Game;
import edu.ntnu.idatt2001.data.action.ItemAction;
import edu.ntnu.idatt2001.data.goal.*;
import edu.ntnu.idatt2001.util.CommonStyles;
import edu.ntnu.idatt2001.util.GameStateFileManager;
import edu.ntnu.idatt2001.data.Player;
import edu.ntnu.idatt2001.data.Story;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for the goals select page.
 * The page contains input fields for the goals the player wants to reach.
 * The player can add as many goals as they want.
 */
public class GoalsSelectPage extends Application {

    private final Player player;
    private final Story story;
    List<Goal> goals;

    /**
     * Constructor for GoalSelectPage.
     *
     * @param player The current player.
     * @param story The selected story.
     */
    public GoalsSelectPage(Player player, Story story) {
        this.player = player;
        this.story = story;
    }

    /**
     * Displays the page.
     *
     * @param stage The stage the page is displayed on.
     */
    @Override
    public void start(Stage stage) {
        VBox rootNode = new VBox(20);
        this.goals = new ArrayList<>();

        Text header = new Text("Select goals");
        Text infoText = new Text("Select goals for the game, then press start game.");
        Text infoText2 = new Text("You can add as many goals as you want.");
        Text infoText3 = new Text("NOTE: If you add more than one item for the inventory-goal it might become " +
                                    "impossible as items may be located in different branches of the story. \n" +
                                    "The stories are currently not very long so don't det to ambitious goals.");
        header.setStyle("-fx-font: 24 arial");

        List<Text> info = List.of(infoText, infoText2, infoText3);
        info.forEach(text -> text.setStyle("-fx-font: 16 arial"));

        Label healthLabel = new Label("Health: ");
        TextField healthField = new TextField();
        healthField.setPromptText("Enter health goal: ");

        Label scoreLabel = new Label("Score: ");
        TextField scoreField = new TextField();
        scoreField.setPromptText("Enter score goal: ");

        Label goldLabel = new Label("Gold: ");
        TextField goldField = new TextField();
        goldField.setPromptText("Enter gold goal: ");

        Label inventoryLabel = new Label("Inventory: ");
        ComboBox<String> itemOptions = new ComboBox<>();
        itemOptions.setPromptText("Select an item to add:");

        List<Label> labels = List.of(healthLabel, scoreLabel, goldLabel, inventoryLabel);
        labels.forEach(label -> label.setStyle("-fx-font: 18 arial"));

        story.getPassages().forEach(passage -> passage.getLinks().forEach(link -> link.getActions().forEach(action -> {
            if (action.getClass().equals(ItemAction.class)) {
                itemOptions.getItems().add(((ItemAction) action).getItem());
            }
        })));

        Button addGoalButton = new Button("Add");
        Button continueButton = new Button("Start Game");
        List<String> inventoryGoalItems = new ArrayList<>();

        addGoalButton.setOnAction(actionEvent -> {
            try {
                if (!healthField.getText().isEmpty() && !healthField.getText().isEmpty()) {
                    int health = Integer.parseInt(healthField.getText());
                    goals.add(new HealthGoal(health));
                }
                if (!scoreField.getText().isEmpty() && !scoreField.getText().isBlank()) {
                    int score = Integer.parseInt(scoreField.getText());
                    goals.add(new ScoreGoal(score));
                }
                if (!goldField.getText().isEmpty() && !goldField.getText().isBlank()) {
                    int gold = Integer.parseInt(goldField.getText());
                    goals.add(new GoldGoal(gold));
                }
                if (itemOptions.getValue() != null) {
                    inventoryGoalItems.add(itemOptions.getValue());
                }

                healthField.clear();
                scoreField.clear();
                goldField.clear();

            } catch (IllegalArgumentException e) {
                Alert inputAlert = new Alert(Alert.AlertType.ERROR);
                inputAlert.setTitle("Invalid input");
                inputAlert.setHeaderText("Invalid input, enter positive integers only.");
                inputAlert.setContentText(e.getMessage());
                inputAlert.show();
            }
        });

        continueButton.setOnAction(event -> {
            Game game = new Game(player, story, goals);
            Player originalPlayer = new Player.Builder(player.getName())
                    .health(player.getHealth())
                    .gold(player.getGold())
                    .score(player.getScore())
                    .build();
            game.setUneditedPlayer(originalPlayer);

            if (!inventoryGoalItems.isEmpty()) {
                goals.add(new InventoryGoal(inventoryGoalItems));
            }

            try {
                GameStateFileManager.saveGameState(game, "src/main/resources/saves/" + player.getName() + ".txt");
            } catch (IOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("Something went wrong when saving the game.");
                alert.showAndWait();
            }


            GamePage gamePage = new GamePage(game);
            Stage gameStage = new Stage();
            gamePage.start(gameStage);
            stage.close();
        });

        CommonStyles.setButtonStyle(16, addGoalButton, continueButton);

        GridPane gridPane = new GridPane();
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.CENTER);

        gridPane.add(healthLabel, 0, 0);
        gridPane.add(healthField, 1, 0);
        gridPane.add(scoreLabel, 0, 1);
        gridPane.add(scoreField, 1, 1);
        gridPane.add(goldLabel, 0, 2);
        gridPane.add(goldField, 1, 2);
        gridPane.add(inventoryLabel, 0, 3);
        gridPane.add(itemOptions, 1, 3);

        rootNode.getChildren().addAll(header, infoText, infoText2, gridPane, addGoalButton, continueButton, infoText3);
        rootNode.setAlignment(Pos.CENTER);

        Scene scene = new Scene(rootNode);
        stage.setMaximized(true);
        stage.setTitle("Game");
        stage.setScene(scene);
        stage.show();
    }
}
