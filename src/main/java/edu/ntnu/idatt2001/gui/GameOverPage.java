package edu.ntnu.idatt2001.gui;

import edu.ntnu.idatt2001.data.Game;
import edu.ntnu.idatt2001.data.Story;
import edu.ntnu.idatt2001.util.CommonStyles;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Class for the game over page.
 * The page appends if the player dies.
 * The player can choose to return to the title screen or restart the game.
 */
public class GameOverPage extends Application {
    private final Game game;
    private final VBox rootNode;

    public GameOverPage(Game game) {
        this.game = game;
        this.rootNode = new VBox();
    }

    /**
     * Displays the game over page.
     *
     * @param stage The stage the page is displayed on.
     */
    @Override
    public void start(Stage stage) {
        rootNode.setSpacing(15);
        rootNode.setAlignment(Pos.CENTER);

        Text endText = new Text("Game Over");
        Text infoText = new Text("You died");

        endText.setStyle("-fx-font: 40 arial");
        infoText.setStyle("-fx-font: 16 arial");

        Button startPageButton = new Button("Return to title");
        Button restartButton = new Button("Restart");

        startPageButton.setOnAction(event -> {
            StartPage startPage = new StartPage();
            startPage.start(new Stage());
            stage.close();
        });

        restartButton.setOnAction(event -> {
            //Pass object over to gamePage.
            Game restartGame = new Game(game.getUneditedPlayer(), game.getStory(), game.getGoals());
            restartGame.setCurrentPassage(game.begin());
            restartGame.setUneditedPlayer(game.getUneditedPlayer());
            GamePage gamePage = new GamePage(restartGame);
            Stage gameStage = new Stage();
            gamePage.start(gameStage);
            stage.close();
        });

        CommonStyles.setButtonStyle(24, startPageButton, restartButton);

        rootNode.getChildren().addAll(endText, infoText, restartButton, startPageButton);

        Scene scene = new Scene(rootNode);
        stage.setMaximized(true);
        stage.setTitle("Paths");
        stage.setScene(scene);
        stage.show();
    }
}