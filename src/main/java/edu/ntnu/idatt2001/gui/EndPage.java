package edu.ntnu.idatt2001.gui;

import edu.ntnu.idatt2001.data.Game;
import edu.ntnu.idatt2001.data.Player;
import edu.ntnu.idatt2001.util.CommonStyles;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.List;

/**
 * Class for the end page.
 * The page contains information about the game, such as goals reached and goals not reached.
 * The player can choose to return to the title screen or restart the game.
 */
public class EndPage extends Application {
    private final VBox rootNode;
    private final Game game;

    /**
     * Constructor for EndPage class.
     *
     * @param game The game that was played.
     */
    public EndPage(Game game) {
        this.game = game;
        this.rootNode = new VBox();
    }

    /**
     * Displays the page.
     *
     * @param stage The stage the page is displayed on.
     */
    @Override
    public void start(Stage stage) {
        rootNode.setSpacing(15);
        rootNode.setAlignment(Pos.CENTER);

        long achievedGoals = game.getGoals().stream()
                .filter(goal -> goal.isFulfilled(game.getPlayer()))
                .count();

        Text endText = new Text("End");
        Text goalAchievedText = new Text("Goals reached: " + achievedGoals);
        Text goalsNotAchieved = new Text("Goals not reached: " + (game.getGoals().size() - achievedGoals));

        Button startPageButton = new Button("Return to title");

        startPageButton.setOnAction(event -> {
            StartPage startPage = new StartPage();
            startPage.start(new Stage());
            stage.close();
        });

        Button restartButton = new Button("Restart");
        restartButton.setOnAction(event -> {
            //Pass object over to gamePage.
            Game restartGame = new Game(game.getUneditedPlayer(), game.getStory(), game.getGoals());
            restartGame.setCurrentPassage(game.begin());
            restartGame.setUneditedPlayer(game.getUneditedPlayer());
            System.out.println(game.getUneditedPlayer().getHealth());

            GamePage gamePage = new GamePage(restartGame);
            Stage gameStage = new Stage();
            gamePage.start(gameStage);
            stage.close();
        });

        endText.setStyle("-fx-font: 40 arial");
        goalAchievedText.setStyle("-fx-font: 16 arial");
        goalsNotAchieved.setStyle("-fx-font: 16 arial");

        List<Button> buttons = List.of(startPageButton, restartButton);
        buttons.forEach(button -> {
            button.prefHeightProperty().bind(Bindings.divide(stage.heightProperty(), 50));
            button.prefWidthProperty().bind(Bindings.divide(stage.widthProperty(), 10));
        });
        CommonStyles.setButtonStyle(24, startPageButton, restartButton);

        rootNode.getChildren().addAll(endText, goalAchievedText, goalsNotAchieved, restartButton, startPageButton);

        Scene scene = new Scene(rootNode);
        stage.setMaximized(true);
        stage.setTitle("Paths");
        stage.setScene(scene);
        stage.show();
    }
}
