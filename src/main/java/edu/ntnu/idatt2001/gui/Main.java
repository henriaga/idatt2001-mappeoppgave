package edu.ntnu.idatt2001.gui;

/**
 * Main class for running the application.
 */
public class Main {
    /**
     * The main method of the application.
     *
     * @param args The commandline arguments.
     */
    public static void main(String[] args) {
        StartPage.main(args);
    }
}
