package edu.ntnu.idatt2001.gui;

import edu.ntnu.idatt2001.data.Player;
import edu.ntnu.idatt2001.data.Story;
import edu.ntnu.idatt2001.util.CommonStyles;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.List;

/**
 * A page that lets the user create a character.
 * The page contains input fields for the characters name, health and score.
 */
public class CreateCharacterPage extends Application {
    private final Story story;

    /**
     * Constructor for class CreateCharacterPage.
     *
     * @param story The selected story.
     */
    public CreateCharacterPage(Story story) {
        this.story = story;
    }

    /**
     * Displays the character creation page.
     *
     * @param stage The stage the page is displayed on.
     */
    @Override
    public void start(Stage stage) {
        VBox rootNode = new VBox(10);

        Text header = new Text("Create character:");
        header.setStyle("-fx-font: 24 arial");

        Image image = new Image("file:src/main/resources/avatars/3.png");
        ImageView imageView = new ImageView(image);

        imageView.setFitWidth(316);
        imageView.setFitHeight(316);
        imageView.setPreserveRatio(true);

        //Label text for input-fields.
        Label nameLabel = new Label("Name:");
        Label healthLabel = new Label("Health:");
        Label goldLabel = new Label("Gold:");
        Label scoreLabel = new Label("Score:");

        List<Label> labels = List.of(nameLabel, healthLabel, goldLabel, scoreLabel);
        labels.forEach(label -> label.setStyle("-fx-font: 18 arial"));

        //Input fields for characters starting health and score.
        TextField nameInput = new TextField();
        nameInput.setPromptText("Input name:");
        TextField healthInput = new TextField();
        healthInput.setPromptText("Input starting health:");
        TextField goldInput = new TextField();
        goldInput.setPromptText("Input starting gold:");
        TextField scoreInput = new TextField();
        scoreInput.setPromptText("Input starting score:");

        //Buttons for returning to select page or creating character.
        Button cancelButton = new Button("Back");
        Button confirmButton = new Button("Create");

        CommonStyles.setButtonStyle(12, cancelButton, confirmButton);

        //Returns to chose character page.
        cancelButton.setOnAction(event -> {
            System.out.println("Returns from CreateCharacterPage to CharacterSelectPage");
            CharacterSelectPage characterSelectPage = new CharacterSelectPage(story);
            characterSelectPage.start(new Stage());
            stage.close();
        });

        //Creates a player.
        confirmButton.setOnAction(event -> {
            try {
                String name = nameInput.getText();
                int health = Integer.parseInt(healthInput.getText());
                int gold = Integer.parseInt(goldInput.getText());
                int score = Integer.parseInt(scoreInput.getText());

                Player player = new Player.Builder(name)
                        .health(health)
                        .gold(gold)
                        .score(score)
                        .build();

                GoalsSelectPage goalPage = new GoalsSelectPage(player, story);
                goalPage.start(new Stage());
                stage.close();
                System.out.println("Player created: " + player.getName());

            } catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Character Creation Error");
                alert.setHeaderText("Invalid character input");
                alert.setContentText("You have entered invalid details for your character.");
                alert.showAndWait();
            }
        });

        GridPane gridPane = new GridPane();
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.CENTER);

        gridPane.add(nameLabel, 0, 0);
        gridPane.add(nameInput, 1, 0);
        gridPane.add(healthLabel, 0, 1);
        gridPane.add(healthInput, 1, 1);
        gridPane.add(goldLabel, 0, 2);
        gridPane.add(goldInput, 1, 2);
        gridPane.add(scoreLabel, 0, 3);
        gridPane.add(scoreInput, 1, 3);
        gridPane.add(cancelButton, 0, 4);
        gridPane.add(confirmButton, 1, 4);

        rootNode.getChildren().addAll(imageView, header, gridPane);
        rootNode.setAlignment(Pos.CENTER);


        Scene scene = new Scene(rootNode);
        stage.setMaximized(true);
        stage.setTitle("Game");
        stage.setScene(scene);
        stage.show();
    }
}
