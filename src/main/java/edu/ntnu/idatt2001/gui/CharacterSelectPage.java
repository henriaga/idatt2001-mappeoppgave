package edu.ntnu.idatt2001.gui;

import edu.ntnu.idatt2001.data.Player;
import edu.ntnu.idatt2001.data.Story;
import edu.ntnu.idatt2001.util.CommonStyles;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The character selection page for the game.
 * <p>On this page the player can chose between three different characters to play as.</p>
 */
public class CharacterSelectPage extends Application {
    private final Story story;
    private Stage stage;
    private final BorderPane rootNode;
    private final VBox topDisplay;
    private final Text header;
    private final Text pageInfo;
    private final VBox centerDisplay;
    private final TextField nameInput;
    private final Label nameLabel;
    private final HBox nameBox;
    private final HBox characterDisplay;
    private final Button createCustomCharacterButton;

    /**
     * Constructor for CharacterSelectPage.
     *
     * @param story The game's story.
     */
    public CharacterSelectPage(Story story) {
        this.story = story;
        this.rootNode = new BorderPane();
        this.topDisplay = new VBox();
        this.header = new Text();
        this.pageInfo = new Text();
        this.centerDisplay = new VBox();
        this.nameInput = new TextField();
        this.nameLabel = new Label();
        this.nameBox = new HBox();
        this.characterDisplay = new HBox();
        this.createCustomCharacterButton = new Button();
    }

    /**
     * Displays the create character page.
     *
     * @param stage The stage the page is displayed on.
     */
    @Override
    public void start(Stage stage) {
        this.stage = stage;

        displayTop();
        displayMiddle();
        displayBottom();

        //Centers button horizontally
        BorderPane.setAlignment(createCustomCharacterButton, Pos.CENTER);

        BorderPane.setMargin(centerDisplay, new Insets(60, 0, 60, 0));
        BorderPane.setMargin(createCustomCharacterButton, new Insets(60, 0, 60, 0));;

        Scene scene = new Scene(rootNode);
        stage.setMaximized(true);
        stage.setTitle("Game");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Adds content to the top of the BorderPane.
     */
    private void displayTop() {
        topDisplay.setAlignment(Pos.CENTER);
        topDisplay.setSpacing(10);

        header.setText("Select a Character");
        pageInfo.setText("Recommended characters");

        header.setStyle("-fx-font: 40 Arial");
        pageInfo.setStyle("-fx-font: 16 Arial");

        topDisplay.getChildren().addAll(header, pageInfo);
        rootNode.setTop(topDisplay);
    }

    /**
     * Adds content to the middle of the BorderPane.
     */
    private void displayMiddle() {
        centerDisplay.setAlignment(Pos.CENTER);
        nameBox.setAlignment(Pos.CENTER);
        nameBox.setPadding(new Insets(0, 0, 40, 0));
        nameBox.setSpacing(10);

        nameLabel.setText("Name:");
        nameInput.setPromptText("Enter name:");
        nameLabel.setStyle("-fx-font: 25 arial");

        nameBox.getChildren().addAll(nameLabel, nameInput);

        createCharacterBoxes();

        centerDisplay.getChildren().addAll(nameBox, characterDisplay);
        rootNode.setCenter(centerDisplay);
    }

    /**
     * Creates the boxes where the characters are displayed.
     */
    private void createCharacterBoxes() {
        characterDisplay.setAlignment(Pos.CENTER);
        centerDisplay.setSpacing(50);

        int[] characterHealth = {100, 200, 70};
        int[] characterGold = {100, 50, 170};
        int[] characterScores = {100, 100, 100};
        String[] characterImages = {"file:src/main/resources/avatars/0.png",
                                    "file:src/main/resources/avatars/1.png",
                                    "file:src/main/resources/avatars/2.png"};

        for (int i = 0; i < 3; i++) {
            VBox characterBox = new VBox(5);

            characterBox.prefWidthProperty().bind(Bindings.divide(centerDisplay.widthProperty(), 8));
            characterBox.setAlignment(Pos.CENTER);

            Image image = new Image(characterImages[i]);
            int health = characterHealth[i];
            int gold = characterGold[i];
            int score = characterScores[i];

            ImageView imageView = new ImageView(image);
            imageView.setFitHeight(316);
            imageView.setFitWidth(316);
            imageView.setPreserveRatio(true);

            Text healthText = new Text("Health: " + health);
            Text goldText = new Text("Gold: " + gold);
            Text scoreText = new Text("Score: " + score);

            healthText.setStyle("-fx-font: 20 Arial");
            goldText.setStyle("-fx-font: 20 Arial");
            scoreText.setStyle("-fx-font: 20 Arial");

            Button selectCharacterButton = new Button("Select");

            selectCharacterButton.prefHeightProperty().bind(Bindings.divide(characterBox.widthProperty(), 20));
            selectCharacterButton.prefWidthProperty().bind(Bindings.divide(characterBox.widthProperty(), 2));
            CommonStyles.setButtonStyle(20, selectCharacterButton);

            selectCharacterButton.setOnAction(event -> {
                try {
                    Player player = new Player.Builder(nameInput.getText())
                            .health(health)
                            .gold(gold)
                            .score(score)
                            .build();

                    GoalsSelectPage goalPage = new GoalsSelectPage(player, story);
                    goalPage.start(new Stage());
                    stage.close();
                    System.out.println("Player created: " + player.getName());
                } catch (IllegalArgumentException e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Character Creation Error");
                    alert.setHeaderText("Something went wrong");
                    alert.showAndWait();

                }
            });

            characterBox.getChildren().addAll(imageView, healthText, goldText, scoreText, selectCharacterButton);
            characterDisplay.getChildren().add(characterBox);
        }
    }

    /**
     * Adds content to the bottom of the BorderPane.
     */
    private void displayBottom() {
        createCustomCharacterButton.setText("Create custom character");

        createCustomCharacterButton.prefHeightProperty().bind(Bindings.divide(stage.widthProperty(), 25));
        createCustomCharacterButton.prefWidthProperty().bind(Bindings.divide(stage.widthProperty(), 3));
        CommonStyles.setButtonStyle(24, createCustomCharacterButton);

        createCustomCharacterButton.setOnAction(event -> {
            System.out.println("Continues from CharacterSelectPage to CreateCharacterPage");
            CreateCharacterPage createCharacterPage = new CreateCharacterPage(story);
            createCharacterPage.start(new Stage());
            stage.close();
        });

        rootNode.setBottom(createCustomCharacterButton);
    }
}
