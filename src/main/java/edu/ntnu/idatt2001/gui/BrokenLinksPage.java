package edu.ntnu.idatt2001.gui;

import edu.ntnu.idatt2001.data.Link;
import edu.ntnu.idatt2001.data.Passage;
import edu.ntnu.idatt2001.data.Story;
import edu.ntnu.idatt2001.util.CommonStyles;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * A page that displays all the broken links in the story.
 * The page contains a button for deleting all the broken links and a button for continuing the game.
 * The page is displayed only if the story contains broken links.
 */
public class BrokenLinksPage extends Application {
    private final Story story;
    private final VBox rootNode;
    private final Text pageInfo;
    private final Text content;
    private final Button deleteBrokenLinksButton;
    private final Button continueButton;

    /**
     * Constructor for class BrokenLinksPage.
     *
     * @param story The selected story.
     */
    public BrokenLinksPage(Story story) {
        this.story = story;
        this.rootNode = new VBox();
        this.pageInfo = new Text();
        this.content = new Text();
        this.deleteBrokenLinksButton = new Button();
        this.continueButton = new Button();
    }

    /**
     * Creates the broken links page.
     *
     * @param stage The stage the page is displayed on.
     */
    @Override
    public void start(Stage stage) {
        rootNode.setSpacing(10);
        rootNode.setAlignment(Pos.CENTER);

        pageInfo.setText("Broken Links:");
        pageInfo.setStyle("-fx-font: 24 arial");

        StringBuilder displayText = new StringBuilder();
        story.getBrokenLinks().forEach(brokenLink -> {
            displayText.append("Link: Text: " + brokenLink.getText() + ", Reference: " + brokenLink.getReference() + "\n");
        });
        content.setText(displayText.toString());
        content.setStyle("-fx-font: 18 arial");

        deleteBrokenLinksButton.setText("Delete Links");
        continueButton.setText("Continue");

        CommonStyles.setButtonStyle(12, deleteBrokenLinksButton, continueButton);

        deleteBrokenLinksButton.setOnAction(event -> {
            //Checks all passages for broken links and removes them.
            for (Link brokenLink : story.getBrokenLinks()) {
                for (Passage passage : story.getPassages()) {
                    passage.getLinks().remove(brokenLink);
                }
            }

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setTitle("Links successfully deleted");
            alert.setContentText("Click Continue to play.");
            alert.showAndWait();
        });

        continueButton.setOnAction(event -> {
            CharacterSelectPage characterSelectPage = new CharacterSelectPage(story);
            characterSelectPage.start(new Stage());
            stage.close();
        });

        rootNode.getChildren().addAll(pageInfo, content, deleteBrokenLinksButton, continueButton);

        Scene scene = new Scene(rootNode);
        stage.setMaximized(true);
        stage.setTitle("Game");
        stage.setScene(scene);
        stage.show();
    }
}
