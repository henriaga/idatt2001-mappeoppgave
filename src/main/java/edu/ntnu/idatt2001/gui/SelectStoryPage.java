package edu.ntnu.idatt2001.gui;

import edu.ntnu.idatt2001.data.Story;
import edu.ntnu.idatt2001.util.CommonStyles;
import edu.ntnu.idatt2001.util.StoryFileManager;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Page for selecting which story to play.
 */
public class SelectStoryPage extends Application {
    private Stage stage;
    private final ListView<String> storyView = new ListView<>();

    /**
     * Displays the selectstorypage.
     *
     * @param stage The stage the page is displayed on.
     */
    @Override
    public void start(Stage stage) {
        this.stage = stage;
        VBox rootNode = new VBox(15);

        fillStoryList();

        Text pageTitle = new Text("Choose Story:");
        Text debuggingInfo = new Text("Note: If the stories aren't showing, check if they are correctly stored. " +
                                        "They should be under: src/main/resources/saves");

        rootNode.setAlignment(Pos.TOP_CENTER);
        pageTitle.setStyle("-fx-font: 24 arial");
        debuggingInfo.setStyle("-fx-font: 16 arial");

        Button selectButton = new Button("Select");
        selectButton.setOnAction(e -> {
            String selectedStory = storyView.getSelectionModel().getSelectedItem();
            if (selectedStory != null) {
                loadStory(selectedStory);
                stage.close();
            }
        });

        Button backButton = new Button("Back");
        backButton.setOnAction(event -> {
            StartPage startPage = new StartPage();
            startPage.start(new Stage());
            stage.close();
        });

        CommonStyles.setButtonStyle(14, selectButton, backButton);
        List<Button> buttons = List.of(selectButton, backButton);
        buttons.forEach(button -> {
            button.prefWidthProperty().bind(Bindings.divide(stage.widthProperty(), 14));
            button.prefHeightProperty().bind(Bindings.divide(stage.heightProperty(), 20));
        });

        rootNode.getChildren().addAll(pageTitle, storyView, selectButton, backButton, debuggingInfo);

        Scene scene = new Scene(rootNode);
        stage.setMaximized(true);
        stage.setTitle("Saved games");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Fills the list with stories.
     */
    public void fillStoryList() {
        try {
            Path saveDir = Paths.get("src/main/resources/storyFiles");
            storyView.getItems().setAll(Files.list(saveDir)
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .collect(Collectors.toList()
                    ));
        } catch (IOException e) {
            StartPage startPage = new StartPage();
            startPage.start(new Stage());
            stage.close();

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Page Loading Error");
            alert.setHeaderText("Error loading page.");
            alert.setContentText("The game might be incorrectly installed.");
            alert.showAndWait();
        }

    }

    /**
     * Loads a story and opens either the brokenlinkspage, the characterselectpage,
     * or displays and alert if there is an error loading.
     *
     * @param storyFileName The name of the .paths file.
     */
    private void loadStory(String storyFileName) {
        try {
            Path path = Paths.get("src/main/resources/storyFiles/" + storyFileName);
            Story story = StoryFileManager.loadStory(path);

            if (!story.getBrokenLinks().isEmpty()) {
                BrokenLinksPage brokenLinksPage = new BrokenLinksPage(story);
                brokenLinksPage.start(new Stage());
                stage.close();
            } else {
                CharacterSelectPage characterSelectPage = new CharacterSelectPage(story);
                characterSelectPage.start(new Stage());
                stage.close();
            }
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Loading Error");
            alert.setHeaderText("Error loading story.");
            alert.setContentText("The story might be save incorrectly. Check if the story is in the storyFiles folder.");
            alert.showAndWait();
        }
    }
}
