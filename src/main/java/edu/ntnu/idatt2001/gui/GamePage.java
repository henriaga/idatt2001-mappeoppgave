package edu.ntnu.idatt2001.gui;

import edu.ntnu.idatt2001.data.*;
import edu.ntnu.idatt2001.util.CommonStyles;
import edu.ntnu.idatt2001.util.GameStateFileManager;
import edu.ntnu.idatt2001.util.SoundController;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Random;

/**
 * The page the player interacts with when playing the game.
 * The page contains a text describing the current event in the story,
 * options for the player to chose between, and information about the player.
 */
public class GamePage extends Application {
    private final Game game;
    private final Player player;
    private Passage currentPassage;
    private SoundController soundController;
    private Random rand;
    private final BorderPane rootNode = new BorderPane();
    private final VBox storyInfo = new VBox(15);
    private final VBox optionBox = new VBox(15);
    private final VBox statDisplay = new VBox(10);
    private final VBox itemDisplay = new VBox(10);
    private final VBox settingsBox = new VBox(10);

    /**
     * Displays the gamepage.
     *
     * @param game The game.
     */
    public GamePage(Game game) {
        this.game = game;
        this.player = game.getPlayer();

        if (game.getCurrentPassage() == null) {
            this.currentPassage = game.begin();
        } else {
            this.currentPassage = game.getCurrentPassage();
        }
    }

    /**
     * Displays the game page.
     *
     * @param stage The stage the page is displayed on.
     */
    @Override
    public void start(Stage stage) {
        rootNode.setPadding(new Insets(60, 0, 0, 0));

        fillStoryInfo(stage);
        storyInfo.setAlignment(Pos.CENTER);
        fillOptionBox(stage);

        fillStatDisplay();
        fillItemDisplay();
        statDisplay.setStyle("-fx-border-color: black; -fx-font: 16 arial; -fx-border-width: 2 1 2 2");
        statDisplay.setPadding(new Insets(10, 10, 10, 20));
        statDisplay.prefWidthProperty().bind(Bindings.divide(stage.widthProperty(), 12));
        itemDisplay.setStyle("-fx-border-color: black; -fx-font: 16 arial; -fx-border-width: 2 2 2 1");
        itemDisplay.setPadding(new Insets(10, 20, 10, 10));
        itemDisplay.prefWidthProperty().bind(Bindings.divide(stage.widthProperty(), 15));

        HBox playerInfo = new HBox();
        playerInfo.getChildren().addAll(statDisplay, itemDisplay);
        playerInfo.setAlignment(Pos.CENTER);

        rootNode.setTop(storyInfo);
        rootNode.setCenter(optionBox);
        rootNode.setBottom(playerInfo);
        rootNode.setRight(settingsBox);

        BorderPane.setAlignment(optionBox, Pos.CENTER);

        settingsBox.setAlignment(Pos.TOP_RIGHT);
        Button saveButton = new Button("Save Game");
        saveButton.setOnAction(event -> {
            try {
                Story savedStory = new Story(game.getStory().getTitle(), game.begin());
                game.getStory().getPassages().forEach(savedStory::addPassage);
                Game savedGame = new Game(player, savedStory, game.getGoals());
                savedGame.setCurrentPassage(currentPassage);
                savedGame.setUneditedPlayer(game.getUneditedPlayer());
                GameStateFileManager.saveGameState(savedGame, "src/main/resources/saves/" + savedGame.getPlayer().getName() + ".txt");
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Game saved");
                alert.setHeaderText(null);
                alert.setContentText("Game saved successfully");
                alert.showAndWait();
            } catch (IOException e) {
                e.printStackTrace();
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("Something went wrong when saving the game");
                alert.showAndWait();
            }
        });

        Button muteSoundButton = new Button("Stop music");
        rand = new Random();
        soundController = new SoundController(rand.nextInt(0,4) + ".wav");
        soundController.playSound();
        muteSoundButton.setOnAction(event -> {
            if (muteSoundButton.getText().equals("Stop music")) {
                soundController.pause();
                muteSoundButton.setText("Play music");
            } else {
                soundController.resume();
                muteSoundButton.setText("Stop music");
            }
        });
        AnchorPane muteSoundPane = new AnchorPane();
        muteSoundPane.setTopAnchor(muteSoundButton, 1.0);
        muteSoundPane.setRightAnchor(muteSoundButton, 10.0);
        muteSoundPane.getChildren().add(muteSoundButton);

        Button titleButton = new Button("Return to title");
        titleButton.setOnAction(event -> {
            StartPage startPage = new StartPage();
            startPage.start(new Stage());
            stage.close();
        });

        settingsBox.getChildren().addAll(saveButton, muteSoundButton, titleButton);

        CommonStyles.setButtonStyle(14, saveButton,muteSoundButton, titleButton);

        Scene scene = new Scene(rootNode);
        stage.setMaximized(true);
        stage.setTitle("Paths");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Fills the statDisplay Vbox with the player's name, health, gold and score.
     */
    private void fillStatDisplay() {
        statDisplay.getChildren().add(new Text(player.getName()));
        statDisplay.getChildren().add(new Text("Health: " + player.getHealth()));
        statDisplay.getChildren().add(new Text("Gold: " + player.getGold()));
        statDisplay.getChildren().add(new Text("Score: " + player.getScore()));
    }

    /**
     * Fills the itemDisplay VBox with the items the player currently has.
     */
    private void fillItemDisplay() {
        Text infoText = new Text("Items:");
        infoText.setStyle("-fx-font: 24 arial");
        itemDisplay.getChildren().add(infoText);
        player.getInventory().forEach(item -> itemDisplay.getChildren().add(new Text(item)));
    }

    /**
     * Fills the optionBox VBox with buttons displaying game choices.
     *
     * @param stage The game window.
     */
    private void fillOptionBox(Stage stage) {
        optionBox.setAlignment(Pos.CENTER);

        if (!currentPassage.hasLinks()) {
            Button endPageButton = new Button("End");

            endPageButton.prefWidthProperty().bind(Bindings.divide(stage.widthProperty(), 1.4));
            endPageButton.prefHeightProperty().bind(Bindings.divide(stage.heightProperty(), 20));
            CommonStyles.setButtonStyle(24, endPageButton);

            endPageButton.setOnAction(event -> {
                soundController.stopSound();
                EndPage endPage = new EndPage(game);
                endPage.start(new Stage());
                stage.close();
            });
            optionBox.getChildren().add(endPageButton);

            return;
        }

        for (Link link : currentPassage.getLinks()) {
            Button optionButton = new Button(link.getText());

            optionButton.prefWidthProperty().bind(Bindings.divide(stage.widthProperty(), 1.4));
            optionButton.prefHeightProperty().bind(Bindings.divide(stage.heightProperty(), 20));
            CommonStyles.setButtonStyle(24, optionButton);

            //Refreshes page with new current passage.
            optionButton.setOnAction(event -> {
                link.getActions().forEach(action -> action.execute(player));

                if (player.isAlive()) {
                    SoundController soundControllerClick = new SoundController("Click.wav");
                    soundControllerClick.playSound();
                    player.addScore(10);
                    this.currentPassage = game.go(link);

                    storyInfo.getChildren().clear();
                    optionBox.getChildren().clear();
                    statDisplay.getChildren().clear();
                    itemDisplay.getChildren().clear();

                    fillStoryInfo(stage);
                    fillOptionBox(stage);
                    fillStatDisplay();
                    fillItemDisplay();
                } else {
                    soundController.stopSound();
                    GameOverPage gameOverPage = new GameOverPage(game);
                    gameOverPage.start(new Stage());
                    stage.close();
                }
            });

            optionBox.getChildren().add(optionButton);
        }
    }

    /**
     * Fills the storyInfo VBox width the current passage's title and content.
     *
     * @param stage The game window.
     */
    private void fillStoryInfo(Stage stage) {
        Text areaText = new Text(currentPassage.getTitle());
        Text storyText = new Text(currentPassage.getContent());
        areaText.setStyle("-fx-font: 24 arial");
        storyText.setStyle("-fx-font: 18 arial");
        storyText.wrappingWidthProperty().bind(Bindings.divide(stage.widthProperty(), 1.6));
        storyInfo.getChildren().addAll(areaText, storyText);
    }
}