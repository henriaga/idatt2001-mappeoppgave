package edu.ntnu.idatt2001.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a player.
 */
public class Player implements Serializable {
    private final String name;
    private int health;
    private int score;
    private int gold;
    private final List<String> inventory;


    /**
     * Constructor for player class.
     *
     * @param builder Builder for player class.
     */
    private Player(Builder builder) {
        if (builder.name == null || builder.name.isEmpty()) {
            throw new IllegalArgumentException("A name must be chosen.");
        }
        if (builder.health <= 0) {
            throw new IllegalArgumentException("Health cannot be less than or equal to 0.");
        }
        if (builder.score < 0) {
            throw new IllegalArgumentException("Score cannot be less than 0.");
        }
        if (builder.gold < 0) {
            throw new IllegalArgumentException("Gold cannot be less than 0.");
        }

        this.name = builder.name;
        this.health = builder.health;
        this.score = builder.score;
        this.gold = builder.gold;
        this.inventory = new ArrayList<>();
    }

    /**
     * Gets the player's name.
     *
     * @return A string containing the player's name.
     */
    public String getName() {
        return name;
    }

    /**
     * Adds amount to player's health.
     * <p>Health will be set to 0 if it falls bellow 0.</p>
     *
     * @param health Amount added to player's health.
     */
    public void addHealth(int health) {
        this.health += health;

        if (this.health < 0) {
            this.health = 0;
        }
    }

    /**
     * Gets the player's health.
     *
     * @return The player's health.
     */
    public int getHealth() {
        return health;
    }

    /**
     * Adds amount to player's score.
     * <p>Score will be set to 0 if it falls bellow 0.</p>
     *
     * @param points Amount added to player's score.
     */
    public void addScore(int points) {
        score += points;

        if (score < 0) {
            score = 0;
        }
    }

    /**
     * Gets the player's score.
     *
     * @return The player's score.
     */
    public int getScore() {
        return score;
    }

    /**
     * Adds amount to player's gold.
     *
     * @param gold Amount added to player's gold.
     */
    public void addGold(int gold) {
        this.gold += gold;

        if (this.gold < 0) {
            this.gold = 0;
        }
    }

    /**
     * Gets the player's gold.
     *
     * @return The player's gold.
     */
    public int getGold() {
        return gold;
    }

    /**
     * Adds an item to the player's inventory.
     *
     * @param item The item added to the player's inventory.
     */
    public void addToInventory(String item) {
        inventory.add(item);
    }

    /**
     * Gets the player's inventory.
     *
     * @return List containing the player's items.
     */
    public List<String> getInventory() {
        return inventory;
    }

    /**
     * Checks if a player is alive.
     *
     * @return {@code false} if the player has 0 health, {@code true} otherwise.
     */
    public boolean isAlive() {
        return health > 0;
    }

    /**
     * Builder class for construction of Player objects.
     */
    public static class Builder {
        //Required parameters
        private final String name;

        //Optional parameters - initialized to default values
        private int health = 100;
        private int score = 0;
        private int gold = 0;

        /**
         * Initializes required parameters.
         *
         * @param name The name of the player.
         */
        public Builder(String name) {
            this.name = name.trim();
        }

        /**
         * Initializes health.
         *
         * @param health The player's health.
         * @return Builder with initialized health value.
         */
        public Builder health(int health) {
            this.health = health;
            return this;
        }

        /**
         * Initializes score
         *
         * @param score The player's score.
         * @return Builder with initialized score.
         */
        public Builder score(int score) {
            this.score = score;
            return this;
        }

        /**
         * Initializes gold.
         *
         * @param gold The player's gold.
         * @return Builder with initialized gold.
         */
        public Builder gold(int gold) {
            this.gold = gold;
            return this;
        }

        /**
         * Creates a Player object.
         *
         * @return Player object with values initialized with builder.
         */
        public Player build() {
            return new Player(this);
        }
    }
}
