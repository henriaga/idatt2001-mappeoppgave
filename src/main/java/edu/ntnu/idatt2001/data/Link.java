package edu.ntnu.idatt2001.data;

import edu.ntnu.idatt2001.data.action.Action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class representing a link between different parts of the story.
 */
public class Link implements Serializable {

    private static final long serialVersionUID = 1L;
    private final String text;
    private final String reference;
    private List<Action> actions;

    /**
     * Creates a link with the specified parameters.
     *
     * @param text Text describing an event or choice in the story.
     * @param reference Unique reference to a passage. Should be the passage's title.
     */
    public Link(String text, String reference) {
        this.text = text;
        this.reference = reference;
        this.actions = new ArrayList<>();
    }

    /**
     * Gets the link's text.
     *
     * @return The link's text as a String.
     */
    public String getText() {
        return text;
    }

    /**
     * Gets the link's reference.
     *
     * @return The link's reference as a String.
     */
    public String getReference() {
        return reference;
    }

    /**
     * Adds an action to the link.
     *
     * @param action The action added to the link.
     */
    public void addAction(Action action) {
        actions.add(action);
    }

    /**
     * Gets the links actions.
     *
     * @return List containing the links actions.
     */
    public List<Action> getActions() {
        return actions;
    }

    /**
     * Returns a string representing the link.
     *
     * @return A string representing the link.
     */
    @Override
    public String toString() {
        return "Link toString" + ": " +
                getText() + ", " +
                getReference();
    }

    /**
     * Checks if two links have the same reference.
     *
     * @param link The link to compare.
     * @return {@code true} if the links have the same reference, {@code false} otherwise.
     */
    public boolean equals(Link link) {
        return this.reference.equals(link.getReference());
    }

    /**
     * Generates a hash value representing the object.
     *
     * @return hash value
     */
    @Override
    public int hashCode() {
        return Objects.hash(text, reference, actions);
    }
}
