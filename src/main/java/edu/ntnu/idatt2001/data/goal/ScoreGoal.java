package edu.ntnu.idatt2001.data.goal;

import edu.ntnu.idatt2001.data.Player;

/**
 * Class representing a goal related to a player's score.
 */
public class ScoreGoal implements Goal {
    private final int minimumPoints;

    /**
     * Creates a ScoreGoal with the specified minimum amount of points.
     *
     * @param minimumPoints The minimum score needed.
     * @throws IllegalArgumentException If minimumPoints is less than 0.
     */
    public ScoreGoal(int minimumPoints) {
        if (minimumPoints < 0) {
            throw new IllegalArgumentException("Goal amount cannot be less than 0.");
        }

        this.minimumPoints = minimumPoints;
    }

    /**
     * Checks if the player has the minimum score needed.
     *
     * @param player The player that will be checked.
     * @return {@code true} if the player has the minimum score, {@code false} otherwise.
     */
    @Override
    public boolean isFulfilled(Player player) {
        return player.getScore() >= minimumPoints;
    }
}
