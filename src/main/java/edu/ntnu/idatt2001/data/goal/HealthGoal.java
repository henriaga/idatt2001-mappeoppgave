package edu.ntnu.idatt2001.data.goal;

import edu.ntnu.idatt2001.data.Player;

/**
 * Class representing a goal relating to a player's health.
 */
public class HealthGoal implements Goal {
    private final int minimumHealth;

    /**
     * Creates a HealthGoal with the specified minimum health.
     *
     * @param minimumHealth The minimum amount of health needed.
     * @throws IllegalArgumentException If minimumHealth is less than 0.
     */
    public HealthGoal(int minimumHealth) {
        if (minimumHealth < 0) {
            throw new IllegalArgumentException("Goal amount cannot be less than 0.");
        }

        this.minimumHealth = minimumHealth;
    }

    /**
     * Checks if a player has the minimum amount of health needed.
     *
     * @param player The player that will be checked.
     * @return {@code true} if the player has the minimum amount of health, {@code false} otherwise.
     */
    @Override
    public boolean isFulfilled(Player player) {
        return player.getHealth() >= minimumHealth;
    }
}
