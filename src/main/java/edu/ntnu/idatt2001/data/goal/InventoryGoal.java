package edu.ntnu.idatt2001.data.goal;

import edu.ntnu.idatt2001.data.Player;

import java.util.HashSet;
import java.util.List;

/**
 * Class representing a goal related to a player's inventory.
 */
public class InventoryGoal implements Goal {
    private final List<String> mandatoryItems;

    /**
     * Creates an InventoryGoal with the specified list of items.
     *
     * @param mandatoryItems A list of the items needed.
     * @throws IllegalArgumentException If mandatoryItems har no elements.
     */
    public InventoryGoal(List<String> mandatoryItems) {
        if(mandatoryItems.isEmpty()) {
            throw new IllegalArgumentException("MandatoryItems cannot be empty.");
        }

        this.mandatoryItems = mandatoryItems;
    }

    /**
     * Checks if a player has the necessary items.
     *
     * @param player The player that will be checked.
     * @return {@code true} if the player has the necessary items, {@code false} otherwise.
     */
    @Override
    public boolean isFulfilled(Player player) {
        return new HashSet<>(player.getInventory()).containsAll(mandatoryItems);
    }
}
