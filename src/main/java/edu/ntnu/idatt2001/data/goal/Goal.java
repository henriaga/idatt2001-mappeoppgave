package edu.ntnu.idatt2001.data.goal;

import edu.ntnu.idatt2001.data.Player;

import java.io.Serializable;

/**
 * Class representing a goal value related to a player's state.
 */
public interface Goal extends Serializable {

    /**
     * Checks if a player has fulfilled a condition.
     *
     * @param player The player that will be checked.
     * @return {@code true} if the condition is fulfilled, {@code false} otherwise.
     */
    boolean isFulfilled(Player player);
}
