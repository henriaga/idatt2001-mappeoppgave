package edu.ntnu.idatt2001.data.goal;

import edu.ntnu.idatt2001.data.Player;

/**
 * Class representing a goal relating to a player's gold.
 */
public class GoldGoal implements Goal {
    private final int minimumGold;

    /**
     * Creates a GoldGoal with the specified minimum gold.
     *
     * @param minimumGold The minimum amount of gold needed.
     * @throws IllegalArgumentException If minimumGold is less than 0.
     */
    public GoldGoal(int minimumGold) {
        if (minimumGold < 0) {
            throw new IllegalArgumentException("Goal amount cannot be less than 0.");
        }

        this.minimumGold = minimumGold;
    }

    /**
     * Checks if a player has the minimum amount of gold needed.
     *
     * @param player The player that will be checked.
     * @return {@code true} if the player has the minimum amount of gold, {@code false} otherwise.
     */
    @Override
    public boolean isFulfilled(Player player) {
        return player.getGold() >= minimumGold;
    }
}
