package edu.ntnu.idatt2001.data.action;

import edu.ntnu.idatt2001.data.Player;

/**
 * Class representing a change to a player's inventory.
 */
public class ItemAction implements Action {
    private final String item;

    /**
     * Creates an ItemAction with the specified item.
     *
     * @param item The item the player will obtain.
     */
    public ItemAction(String item) {
        this.item = item;
    }

    /**
     * Adds an item to the players inventory.
     *
     * @param player The player that will be changed.
     */
    @Override
    public void execute(Player player) {
        player.addToInventory(item);
    }

    /**
     * Gets the action's item.
     *
     * @return The item.
     */
    public String getItem() {
        return item;
    }
}
