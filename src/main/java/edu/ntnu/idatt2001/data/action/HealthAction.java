package edu.ntnu.idatt2001.data.action;

import edu.ntnu.idatt2001.data.Player;

/**
 * Class representing a change to a player's health.
 */
public class HealthAction implements Action {
    private final int health;

    /**
     * Creates a HealthAction with the specified health.
     *
     * @param health The amount of health the player will gain/lose.
     */
    public HealthAction(int health) {
        this.health = health;
    }

    /**
     * Adds or removes health from a player.
     *
     * @param player The player that will be changed.
     */
    @Override
    public void execute(Player player) {
        player.addHealth(health);
    }
}
