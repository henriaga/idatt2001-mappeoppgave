package edu.ntnu.idatt2001.data.action;

import edu.ntnu.idatt2001.data.Player;
import java.io.Serializable;

/**
 * Class representing a change to a player.
 */
public interface Action extends Serializable {
    /**
     * Changes the state of the player class.
     *
     * @param player The player that will be changed.
     */
    void execute(Player player);
}
