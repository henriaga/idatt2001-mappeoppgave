package edu.ntnu.idatt2001.data.action;

import edu.ntnu.idatt2001.data.Player;

/**
 * Class representing a change to a player's gold.
 */
public class GoldAction implements Action {
    private final int gold;

    /**
     * Creates a GoldAction with the specified gold.
     *
     * @param gold The amount of gold the player will gain/lose.
     */
    public GoldAction(int gold) {
        this.gold = gold;
    }

    /**
     * Adds or removes a player's gold.
     *
     * @param player The player that will be changed.
     */
    @Override
    public void execute(Player player) {
        player.addGold(gold);
    }
}