package edu.ntnu.idatt2001.data.action;

import edu.ntnu.idatt2001.data.Player;

/**
 * Class representing a change to a player's score.
 */
public class ScoreAction implements Action {

    private final int points;

    /**
     * Creates a ScoreAction with the specified points.
     *
     * @param points How many points the player will gain/lose.
     */
    public ScoreAction(int points) {
        this.points = points;
    }

    /**
     * Adds or removes points from a player.
     *
     * @param player The player that will be changed.
     */
    @Override
    public void execute(Player player) {
        player.addScore(points);
    }
}
