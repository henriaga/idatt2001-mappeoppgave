package edu.ntnu.idatt2001.data;

import edu.ntnu.idatt2001.data.Link;
import edu.ntnu.idatt2001.data.Passage;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class representing a story.
 * <p>A collection of links of passages creating a non-linear, interactive narrative.</p>
 */
public class Story implements Serializable {
    private final String title;
    private final Map<Link, Passage> passages;
    private Passage openingPassage;

    /**
     * Creates a story with the specified title and opening passage, also initializes a hashmap containing passages.
     *
     * @param title The story's title.
     * @param openingPassage The first passage in the story.
     */
    public Story(String title, Passage openingPassage) {
        this.title = title;
        this.passages = new HashMap<>();
        this.openingPassage = openingPassage;
    }

    /**
     * Creates a story with the specified title, also initializes a hashmap containing passages.
     * Sets openingpassage to null, and must be set later.
     *
     * @param title The story's title.
     */
    public Story(String title) {
        this.title = title;
        this.passages = new HashMap<>();
        this.openingPassage = null;
    }

    /**
     * Sets the opening passage.
     *
     * @param openingPassage The first passage that is displayed.
     */
    public void setOpeningPassage(Passage openingPassage) {
        this.openingPassage = openingPassage;
    }

    /**
     * Gets the title of the story.
     *
     * @return The title of the story.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets the first passage of the story.
     *
     * @return The first passage of the story.
     */
    public Passage getOpeningPassage() {
        return openingPassage;
    }

    /**
     * Adds a passage to the story.
     * <p>Adds a passage to a HashMap of passages. The key is a link created using the passage's title as its title and reference.</p>
     *
     * @param passage The passage added to the story.
     */
    public void addPassage(Passage passage) {
        passages.put(new Link(passage.getTitle(), passage.getTitle()), passage);
    }

    /**
     * Gets the passage with the title corresponding to the link's reference.
     *
     * @param link A link with the reference to the passage.
     * @return The passage with the title corresponding the link's reference. Null if no such passage exists.
     */
    public Passage getPassage(Link link) {
        for (var entry : passages.entrySet()) {
            if (entry.getKey().equals(link)) {
                return entry.getValue();
            }
        }

        return null;
    }

    /**
     * Gets a Collection of all the story's passages.
     *
     * @return A Collection of the story's passages.
     */
    public Collection<Passage> getPassages() {
        return passages.values();
    }

    /**
     * Removes a passage if it isn't referenced.
     *
     * @param link The key to the passage.
     * @return {@code true} if the passage is deleted, {@code false} otherwise.
     */
    public boolean removePassage(Link link) {
        if (isPassageReferenced(getPassage(link))) {
            return false;
        }

        passages.remove(link);
        return true;
    }

    /**
     * Checks if any link links to the specific passage.
     *
     * @param passage The passage to check for references.
     * @return {@code true} if the passage is referenced by any link, {@code false} otherwise.
     */
    public boolean isPassageReferenced(Passage passage) {
        return passages.values().stream()
                .flatMap(p -> p.getLinks().stream())
                .anyMatch(link -> link.getReference().equals(passage.getTitle()));
    }

    /**
     * Gets a list of all the links in the story that do not reference any passage.
     *
     * @return A list of all the links in the story that do not reference any passage.
     */
    public List<Link> getBrokenLinks() {
        return passages.values().stream()
                .flatMap(passage -> passage.getLinks().stream())
                .filter(link -> getPassage(link) == null)
                .collect(Collectors.toList());
    }
}
