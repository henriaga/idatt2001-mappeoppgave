package edu.ntnu.idatt2001.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class representing a passage (part of the story).
 */
public class Passage implements Serializable {
    private final String title;
    private final String content;
    private final List<Link> links;

    /**
     * Creates a passage with the specified parameters.
     *
     * @param title The name of the passage, describes the passage and works as an identificator.
     * @param content Content of the passage, typically a paragraph or dialogue.
     */
    public Passage (String title, String content) {
        this.title = title;
        this.content = content;
        this.links = new ArrayList<>();
    }

    /**
     * Gets the passage's title.
     *
     * @return The passage's title as a String.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets the passage's content.
     *
     * @return The passage's content as a String.
     */
    public String getContent() {
        return content;
    }

    /**
     * Adds a link that connects the passage to another passage.
     *
     * @param link The link that will be added to the passage.
     */
    public void addLink(Link link) {
        links.add(link);
    }

    /**
     * Gets the links that connects the passage to other passages.
     *
     * @return List containing the passages.
     */
    public List<Link> getLinks() {
        return links;
    }

    /**
     * Checks if the passages has any links.
     *
     * @return {@code true} if the passage has at least one link, {@code false} otherwise.
     */
    public boolean hasLinks() {
        return !links.isEmpty();
    }

    /**
     * Returns a String representing the passage.
     *
     * @return A String containing the passage's information.
     */
    @Override
    public String toString() {
        return "Passage{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", links=" + links +
                '}';
    }

    /**
     * Checks if the passage is equal to another object.
     *
     * @param o The other object being compared.
     * @return {@code true} if the objects are equal, {@code false} otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Passage passage = (Passage) o;
        return title.equals(passage.title) && content.equals(passage.content) && Objects.equals(links, passage.links);
    }

    /**
     * Generates a hash value representing the object.
     *
     * @return hash value
     */
    @Override
    public int hashCode() {
        return Objects.hash(title, content, links);
    }
}
