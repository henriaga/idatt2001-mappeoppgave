package edu.ntnu.idatt2001.data;

import edu.ntnu.idatt2001.data.goal.Goal;

import java.io.Serializable;
import java.util.List;

/**
 * Class acting as a facade for the game, hiding complexity.
 */
public class Game implements Serializable {
    private final Player player;
    private final Story story;
    private final List<Goal> goals;
    private Passage currentPassage;
    private Player uneditedPayer;

    /**
     * Creates a game with the specified parameters.
     *
     * @param player The game's player.
     * @param story The game's story.
     * @param goals The goals of the game.
     */
    public Game(Player player, Story story, List<Goal> goals) {
        this.player = player;
        this.story = story;
        this.goals = goals;
    }

    /**
     * Gets the player.
     *
     * @return The game's player.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Gets the story.
     *
     * @return The game's story.
     */
    public Story getStory() {
        return story;
    }

    /**
     * Gets the goals of the game.
     *
     * @return The game's goals.
     */
    public List<Goal> getGoals() {
        return goals;
    }

    /**
     * Returns the story's first passage.
     *
     * @return The story's opening passage.
     */
    public Passage begin() {
        return story.getOpeningPassage();
    }

    /**
     * Goes to a passage given the specified link.
     *
     * @param link The key to the passage.
     * @return The passage with the link as a key.
     */
    public Passage go(Link link) {
        return story.getPassage(link);
    }

    /**
     * Gets the current passage.
     *
     * @return The current passage.
     */
    public Passage getCurrentPassage() {
        return currentPassage;
    }

    /**
     * Sets the current passage.
     *
     * @param currentPassage The passage to set as current.
     */
    public void setCurrentPassage(Passage currentPassage) {
        this.currentPassage = currentPassage;
    }

    /**
     * Returns the original player.
     *
     * @return The player with the stats before they were changed.
     */
    public Player getUneditedPlayer() {
        return uneditedPayer;
    }

    /**
     * Method to store the original state of the player.
     *
     * @param uneditedPayer The player before any stats were changed.
     */
    public void setUneditedPlayer(Player uneditedPayer) {
        this.uneditedPayer = uneditedPayer;
    }
}
