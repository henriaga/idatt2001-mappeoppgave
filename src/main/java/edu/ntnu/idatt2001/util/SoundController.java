package edu.ntnu.idatt2001.util;

import javax.sound.sampled.Clip;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import java.io.File;
import java.io.IOException;


/**
 * Class for handling sound effects.
 */
public class SoundController {
    private volatile boolean isPlaying = false;
    private Clip clip;
    private static final String SOUNDS_PATH = "src/main/resources/soundFX/";

    /**
     * Constructor for SoundController.
     * Loads the sound file.
     *
     * @param fileName The path to the sound file.
     */
    public SoundController(String fileName) {
    fileName = SOUNDS_PATH + fileName;
        Clip tempClip;
        try {
            File soundFile = new File(fileName);
            tempClip = AudioSystem.getClip();
            tempClip.open(AudioSystem.getAudioInputStream(soundFile));

        } catch (IOException | UnsupportedAudioFileException | LineUnavailableException e) {
            throw new RuntimeException("Error loading sound file: " + fileName, e);
        }
        clip = tempClip;
    }
    /**
     * Plays a sound effect.
     */
    public void playSound() {
        isPlaying = true;
        FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
        volume.setValue(-20.0f);
        clip.setFramePosition(0);
        new Thread(() -> {
            clip.start();

            while(true){
                if(clip.getFramePosition() == clip.getFrameLength()){
                    break;
                } if(!isPlaying){
                    break;
                }
            }
        }).start();
    }

    /**
     * Pauses the sound effect.
     */
    public void pause() {
        isPlaying = false;
        clip.stop();
    }

    /**
     * Resumes the sound effect.
     */
    public void resume() {
        isPlaying = true;
        new Thread(() -> {
            clip.start();
            while(true){
                if(clip.getFramePosition() == clip.getFrameLength()){
                    break;
                } if(!isPlaying){
                    break;
                }
            }
        });
        clip.start();
    }

    /**
     * Stops the sound effect.
     */
    public void stopSound() {
        isPlaying = false;
        clip.setFramePosition(0);
    }

    /**
     * Checks if the sound effect is playing.
     *
     * @return True if the sound effect is playing, false if not.
     */
    public boolean isPlaying() {
        return isPlaying;
    }
}
