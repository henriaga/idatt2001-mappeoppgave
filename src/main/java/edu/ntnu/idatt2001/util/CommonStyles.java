package edu.ntnu.idatt2001.util;

import javafx.scene.control.Button;

/**
 * Class with static methods for commonly used styles.
 */
public class CommonStyles {
    /**
     * Applies the common button style to buttons.
     *
     * @param fontSize The fontsize that will be applied.
     * @param buttons The buttons the style will be applied to.
     */
    public static void setButtonStyle(int fontSize, Button... buttons) {
        for (Button button : buttons) {
            button.setStyle("-fx-background-color: grey; -fx-border-color: black; -fx-font: " + fontSize
                    + " arial; -fx-text-fill: white");
            button.hoverProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue) {
                    button.setStyle("-fx-background-color: black; -fx-font: " + fontSize
                            + " arial; -fx-text-fill: white");
                } else {
                    button.setStyle("-fx-background-color: grey; -fx-border-color: black; -fx-font: " + fontSize
                            + " arial; -fx-text-fill: white");
                }
            });
        }
    }
}
