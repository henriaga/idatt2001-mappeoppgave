package edu.ntnu.idatt2001.util;

import edu.ntnu.idatt2001.data.Link;
import edu.ntnu.idatt2001.data.Passage;
import edu.ntnu.idatt2001.data.Story;
import edu.ntnu.idatt2001.data.action.Action;

import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;


/**
 * Class for reading and writing stories to and from files.
 */
public class StoryFileManager {
    private static final String ACTIONS_REGEX = "\\[(.*?)\\]\\((.*?)\\)\\{(.*?)\\}";
    private static final String NO_ACTIONS_REGEX = "\\[(.*?)\\]\\((.*?)\\)";

    /**
     * Saves the story to the specified file path.
     *
     * @param story    the story to save
     * @param filePath the file path to save the story to
     * @throws IOException if an I/O error occurs
     */
    public static void saveStory(Story story, Path filePath) throws IOException {
        if (!filePath.toString().endsWith(".paths")) {
            filePath = filePath.resolveSibling(filePath.getFileName() + ".paths");
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath.toFile()))) {
            writer.write(story.getTitle());
            writer.newLine();
            writer.newLine();

            for (Passage passage : story.getPassages()) {
                writer.write("::" + passage.getTitle());
                writer.newLine();
                writer.write(passage.getContent());
                writer.newLine();

                for (Link link : passage.getLinks()) {
                    writer.write("[" + link.getText() + "](" + link.getReference() + ")");

                    if (!link.getActions().isEmpty()) {
                        writer.write("{" + actionsToString(link.getActions()) + "}");
                    }
                    writer.newLine();
                }
                writer.newLine();
            }
        } catch (IOException e) {
            throw new IOException("Could not save story", e);
        }
    }

    /**
     * Loads a story from the specified file path.
     *
     * @param filePath the file path to load the story from
     * @return the loaded story
     * @throws IOException if an I/O error occurs
     */
    public static Story loadStory(Path filePath) throws IOException {
        if (!filePath.toString().endsWith(".paths")) {
            filePath = filePath.resolveSibling(filePath.getFileName() + ".paths");
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath.toFile()))) {
            String title = reader.readLine();
            Story story = new Story(title);
            reader.readLine(); // Skip the empty line

            String line;
            Passage currentPassage = null;
            boolean isFirstPassage = true;

            while ((line = reader.readLine()) != null) {
                if (line.startsWith("::")) {
                    currentPassage = new Passage(line.substring(2), reader.readLine());
                    story.addPassage(currentPassage);

                    // Set the opening passage if it's the first passage
                    if (isFirstPassage) {
                        story.setOpeningPassage(currentPassage);
                        isFirstPassage = false;
                    }

                //Loads the content from the line if there is no actions
                } else if (line.matches(NO_ACTIONS_REGEX)) {
                    String[] linkTextAndReference = line.split("\\]\\(");
                    String linkText = linkTextAndReference[0].substring(1);
                    String linkReference = linkTextAndReference[1].substring(0, linkTextAndReference[1].length() - 1);
                    Link link = new Link(linkText, linkReference);

                    if (currentPassage == null) throw new AssertionError();
                    currentPassage.addLink(link);

                //Loads the content from the line if there is actions
                } else if (line.matches(ACTIONS_REGEX)) {
                    String[] linkParts = line.split("\\{");
                    String[] linkTextAndReference = linkParts[0].split("\\]\\(");
                    String linkText = linkTextAndReference[0].substring(1);
                    String linkReference = linkTextAndReference[1].substring(0, linkTextAndReference[1].length() - 1);
                    Link link = new Link(linkText, linkReference);

                    //Checks if there is actions, and adds them to the link.
                    if (linkParts.length > 1) {
                        List<Action> actions = actionsFromString(linkParts[1].substring(0, linkParts[1].length() - 1));
                        actions.forEach(link::addAction);
                    }
                    if (currentPassage == null) throw new AssertionError();
                    currentPassage.addLink(link);

                } else if (line.isEmpty()) {
                     //Skip empty lines
                } else {
                    throw new IOException("Invalid file format");
                }
            }

            return story;

        } catch (IOException e) {
            throw new IOException("Could not load story: " + e);
        }
    }

    /**
     * Converts a list of actions to a string.
     *
     * @param actions the list of actions to convert
     * @return the string representation of the actions
     */
    private static String actionsToString(List<Action> actions) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < actions.size(); i++) {
            sb.append(actions.get(i).toString());
            if (i < actions.size() - 1) {
                sb.append(";");
            }
        }
        return sb.toString();
    }

    /**
     * Converts a string to a list of actions.
     *
     * @param actionsString the string to convert
     * @return the list of actions
     */
    private static List<Action> actionsFromString(String actionsString) {
        List<Action> actions = new ArrayList<>();
        String[] actionStrings = actionsString.split(";");
        for (String actionString : actionStrings) {
            Action action = ActionFactory.createActionFromString(actionString);
            if (action != null) {
                actions.add(action);
            }
        }
        return actions;
    }
}
