package edu.ntnu.idatt2001.util;

import edu.ntnu.idatt2001.data.action.Action;
import edu.ntnu.idatt2001.data.action.GoldAction;
import edu.ntnu.idatt2001.data.action.HealthAction;
import edu.ntnu.idatt2001.data.action.ItemAction;
import edu.ntnu.idatt2001.data.action.ScoreAction;

/**
 * Creates actions.
 */
public class ActionFactory {

    /**
     * Returns a new action with the specified type and value.
     *
     * @param actionString A string containing the action type and value in the following format "Type:Value".
     * @return An action of the specified type with the specified value. Null if the action type or value is invalid.
     */
    public static Action createActionFromString(String actionString) {
        if (actionString == null || actionString.isEmpty() || actionString.isBlank()) {
            return null;
        }

        String[] parts = actionString.split(":");

        if (parts.length != 2) {
            return null;
        }

        String actionType = parts[0].trim();
        String actionValue = parts[1].trim();

        switch (actionType) {
            case "Gold" -> {
                try {
                    int gold = Integer.parseInt(actionValue);
                    return new GoldAction(gold);
                } catch (NumberFormatException e) {
                    return null;
                }
            }
            case "Health" -> {
                try {
                    int health = Integer.parseInt(actionValue);
                    return new HealthAction(health);
                } catch (NumberFormatException e) {
                    return null;
                }
            }
            case "Item" -> {
                return new ItemAction(actionValue);
            }
            case "Score" -> {
                try {
                    int points = Integer.parseInt(actionValue);
                    return new ScoreAction(points);
                } catch (NumberFormatException e) {
                    return null;
                }
            }
            default -> {
                return null;
            }
        }
    }
}

