package edu.ntnu.idatt2001.util;

import edu.ntnu.idatt2001.data.Game;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;


/**
 * Class for reading and writing the game state to and from files i.e. state of the player, story and goals.
 */
public class GameStateFileManager {

    /**
     * Saves the game state to the specified file path.
     *
     * @param gameState the game state to save
     * @param path      the file path to save the game state to
     * @throws IOException if an I/O error occurs
     */
    public static void saveGameState(Game gameState, String path) throws IOException {
        File file = new File(path);
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))) {
            out.writeObject(gameState);
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("File not found");
        }
    }

    /**
     * Loads a game state from the specified file path.
     *
     * @param path the file path to load the game state from
     * @return the game state
     * @throws IOException            if an I/O error occurs
     * @throws ClassNotFoundException if the class of the serialized object cannot be found
     */
    public static Game loadGameState(String path) throws IOException, ClassNotFoundException {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(path))) {
            return (Game) in.readObject();
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("File not found");
        }
    }
}
